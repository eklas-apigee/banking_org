var routingHost = context.getVariable("routing.host");
print(routingHost);
var routingUrl = context.getVariable("routing.path");
print (routingUrl);

var svbSecurityContext = JSON.parse(context.getVariable("x-svb-security-context"));
context.setVariable("request.header.x-svb-security-context", JSON.stringify(svbSecurityContext));

context.setVariable("target.url", routingHost + routingUrl );